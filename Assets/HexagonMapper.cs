﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonMapper : MonoBehaviour
{
    [HideInInspector]
    public static int gridHeight,gridWidth;

    private GameObject hexagonPrefab;
    private GameObject clickPointPrefab;

    public static GameObject[,] allHexagons;


    internal float xOffset;
    internal float yOffset;
    
    // I decided to create the grid based on a fixed coordinate system instead of building on hexagons
    // grid height is double the hexagon count on y axis because I am putting hexagons every even Y coordinate
    // this makes the grid like this --> for every x and y both even there is one hexagon && for every x and y both odd there is one hexagon

    // this was supposed to make indexing easier because no matter where you are coordinates for neighbours would be the same
    // Like --> top hexagon is y+2 bottom one is y-2 top left is x-1,y+1 top right is x+1,y-1 etc.

    public void CreateHexagons()
    {
        xOffset = 0.79f;
        yOffset = 0.465f;

        //getting variables from game manager
        hexagonPrefab = GameManager.Instance.hexagonPrefab;
        clickPointPrefab = GameManager.Instance.clickPointPrefab;
        gridHeight = GameManager.Instance.gridHeight;
        gridWidth = GameManager.Instance.gridWidth;

        // array for storing all hexagons
        allHexagons = new GameObject[gridWidth, gridHeight*2];

        transform.position = new Vector2((gridWidth / 2 + 2f) / 2, (gridHeight / 2) + 0.5f); // Centering the grid object by reference from hexagons
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight * 2 ; y++)
            {
                float yPosition = y * yOffset;
                float xPosition = x * xOffset;
                
                if (y % 2 == 0 && x % 2 == 0)
                {
                    GameObject hex = Instantiate(hexagonPrefab, new Vector2(xPosition, yPosition), Quaternion.identity);
                    hex.transform.SetParent(gameObject.transform);
                    hex.name = "Hex(" + x + "," + y + ")";
                    hex.GetComponent<Hexagon>().row = x;
                    hex.GetComponent<Hexagon>().column = y;
                    allHexagons[x, y] = hex;

                    
                }
                else if (y % 2 == 1 && x % 2 == 1)
                {
                    GameObject hex = Instantiate(hexagonPrefab, new Vector2(xPosition, yPosition), Quaternion.identity);
                    hex.transform.SetParent(gameObject.transform);
                    hex.name = "Hex(" + x + "," + y + ")";
                    hex.GetComponent<Hexagon>().row = x;
                    hex.GetComponent<Hexagon>().column = y;
                    allHexagons[x, y] = hex;

                    
                    // putting the clickable points on every hexagons top 4 corners 
                    if (x > 0 && x < gridWidth - 1 && y < gridHeight * 2 - 1 && y > 0)
                    {
                        CreatePointAtPosition(x, y, xPosition - (2 * xOffset / 3), yPosition);
                        CreatePointAtPosition(x, y, xPosition + (2 * xOffset / 3), yPosition);
                        CreatePointAtPosition(x, y, xPosition + (xOffset / 3), yPosition + yOffset);
                        CreatePointAtPosition(x, y, xPosition - (xOffset / 3), yPosition + yOffset);
                    }
                }


            }
        }
        CenterTheGame();
    }

    private void CreatePointAtPosition(int x, int y, float xPosition, float yPosition)
    {
        GameObject point = Instantiate(clickPointPrefab, new Vector2(xPosition, yPosition), Quaternion.identity);
        point.transform.SetParent(gameObject.transform);
        point.name = "Point(" + x + "," + y + ")";
    }

    public void CenterTheGame()
    {
        transform.position = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
        
    }
}
