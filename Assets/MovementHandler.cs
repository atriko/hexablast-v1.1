﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHandler : MonoBehaviour
{
    List<Hexagon> destroyedHexagons = new List<Hexagon>();
    // Start is called before the first frame update
    void Start()
    {
        Hexagon.ChangingPositions += ChangePosition;
        Hexagon.DestroyMatchingHexagons += DestroyMatching;
    }
    
    private void ChangePosition(string direction) // calling change position method from hexagons with 3 selected hexagon 
    {

        if (direction == "Up")
        {
            Point.nearestHexes[0].ChangePosition(Point.nearestHexes[1]);
            Point.nearestHexes[1].ChangePosition(Point.nearestHexes[2]);
            Point.nearestHexes[2].ChangePosition(Point.nearestHexes[0]);
        }
        else if (direction == "Down")
        {
            Point.nearestHexes[0].ChangePosition(Point.nearestHexes[2]);
            Point.nearestHexes[1].ChangePosition(Point.nearestHexes[0]);
            Point.nearestHexes[2].ChangePosition(Point.nearestHexes[1]);
        }
        
    }
    private void DestroyMatching() // find matching hexagons from all hexes and destroy them 
    {
        foreach (GameObject item in HexagonMapper.allHexagons)
        {
            if (item != null && item.GetComponent<Hexagon>().isMatched)
            {
                destroyedHexagons.Add(item.GetComponent<Hexagon>()); // add destroyed hexagons to a list for further use
                Destroy(item);
            }
        }
        StartCoroutine(DecreaseColumns());
    }

    private IEnumerator DecreaseColumns()
    {
        yield return new WaitForSeconds(0.4f);
        // Not implemented 
    }

    private void DecreaseOneColumn(int columnNumber, int missingInColumn, int missingObjectRow)
    {
        

    }
}
