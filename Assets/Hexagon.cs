﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hexagon : MonoBehaviour
{
    public static Action<string> ChangingPositions = delegate { };
    public static Action DestroyMatchingHexagons = delegate { };

    public int row;
    public int column;
    public bool isMatched;
    public bool isSelected;
    public bool isRotating;
    public Color color;

    private Color[] colors = { Color.red, Color.blue, Color.green, Color.yellow, Color.cyan, Color.magenta };

    private Hexagon[] neighbours = new Hexagon[6];
    private bool canSwipe;
    private float swipeAngle;

    Vector2 firstTouchPosition;
    Vector2 finalTouchPosition;

    [SerializeField]
    private GameObject border = default;
    private SpriteRenderer mySpriteRenderer;
    private int targetRow;
    private int targetColumn;
    private float targetX;
    private float targetY;
    private Vector2 targetPos;

    private void Start()
    {
        mySpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        isMatched = false;
        PickColor();
        targetRow = row;
        targetColumn = column;
    }

    private void PickColor() // Will pick a random color based on color count from game manager at starting
    {
        int randomColorNumber = UnityEngine.Random.Range(0, GameManager.Instance.colorCount);
        color = colors[randomColorNumber];
        mySpriteRenderer.color = color;
    }

    public void Selected()
    {
        isSelected = true;
        border.SetActive(true);
    }
    public void Deselected()
    {
        isSelected = false;
        border.SetActive(false);
    }
    public void ChangePosition(Hexagon targetHexagon) // changing positions with a designated hexagon
    {
        targetRow = targetHexagon.row;
        targetColumn = targetHexagon.column;
        targetX = targetHexagon.transform.position.x;
        targetY = targetHexagon.transform.position.y;
        targetPos = targetHexagon.transform.position;
        isRotating = true;
    }
    private void OnMouseDown()
    {
        firstTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    private void OnMouseUp()
    {
        finalTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        CalculateAngle();
    }
    private void Update()
    {
        // updating your row and column always so it can be fixed when you are changing with another hexagon
        row = targetRow; 
        column = targetColumn;
        if (isRotating)
        {
            // Lerping to the new position
            float newX = Mathf.Lerp(transform.position.x, targetX, Time.deltaTime * 20f);
            float newY = Mathf.Lerp(transform.position.y, targetY, Time.deltaTime * 20f);
            transform.position = new Vector2(newX, newY);

            // stop when you reach your destination
            if (Vector2.Distance(transform.position, targetPos) < 0.001f)
            {
                //updating your indexing after rotation
                if (HexagonMapper.allHexagons[row, column] != gameObject)
                {
                    HexagonMapper.allHexagons[row, column] = gameObject;
                }
                isRotating = false;

                FindNeighbours();
                
            }
        }
    }


    private void CalculateAngle()
    {
        //calculate angle from swiping and use it to set a direction for changing
        swipeAngle = Mathf.Atan2(finalTouchPosition.y - firstTouchPosition.y, finalTouchPosition.x - firstTouchPosition.x) * 180 / Mathf.PI;
        if (isSelected && !isRotating)
        {

            if (swipeAngle < 180 && swipeAngle > 0)
            {
                ChangingPositions("Up");
                //changeDirection = "Up";
            }
            else
            {
                ChangingPositions("Down");
                //changeDirection = "Down";

            }
        }

    }
    public void FindNeighbours() // To find Neighbours Raycasting to the every 30 + i*60 degrees 6 times and getting the first hexagon it touches
    {
        gameObject.layer = 0; // changing self layer to default so raycast doesnt detect itself
        for (int i = 0; i < 6; i++)
        {
            Vector2 direction = GetDirectionVector2D(30f + (i * 60f));
            RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, direction, 2f, 1 << 8);
            if (hit.collider != null)
            {
                neighbours[i] = (hit.collider.gameObject.GetComponent<Hexagon>());
            }
        }
        gameObject.layer = 8; // changing layer back to hexagon
        CheckMatching();
    }

    private void CheckMatching() // checking consecutive neighbours for matching colors
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            int a = i + 1;
            if (i == neighbours.Length - 1 ) // if its the last neighbour you are looking next consecutive hexagon is on zero
            {
                a = 0;
            }
            if (neighbours[a] != null && neighbours[i] != null) // as long as there is something to check you should check
            {
                if (color == neighbours[i].color && color == neighbours[a].color)
                {
                    isMatched = true;
                    neighbours[i].isMatched = true;
                    neighbours[a].isMatched = true;
                }
            }
            
        }
        DestroyMatchingHexagons(); // calling the destroy event after marking the matches
    }

    public static Vector2 GetDirectionVector2D(float angle)
    {
        return new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad)).normalized;
    }

}



