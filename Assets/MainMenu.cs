﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public TextMeshProUGUI heightText;
    public TextMeshProUGUI widthText;
    public TextMeshProUGUI colorCountText;

    public Slider heightSlider;
    public Slider widthSlider;
    public Slider colorCountSlider;
    
    
    void Update() // just updating game manager variables based on slider values
    {
        heightText.text = "Grid Height:" + heightSlider.value.ToString();
        widthText.text = "Grid Width:" + widthSlider.value.ToString();
        colorCountText.text = "Color Count:" + colorCountSlider.value.ToString();

        GameManager.Instance.gridHeight = (int)heightSlider.value;
        GameManager.Instance.gridWidth = (int)widthSlider.value;
        GameManager.Instance.colorCount = (int)colorCountSlider.value;
    }
}
