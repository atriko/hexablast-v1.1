﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    public static Hexagon[] nearestHexes = new Hexagon[3];

    private void OnMouseDown()
    {
        DeselectAll();// deselect before new selection
        for (int i = 0; i < 3; i++) // shooting 3 rays around your click point to get the selected hexagons
        {
            Vector2 direction = GetDirectionVector2D(30f + (i * 120f));
            RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, direction, 2f, 1 << 8);
            if (hit.collider != null)
            {
                hit.collider.gameObject.GetComponent<Hexagon>().Selected();
                nearestHexes[i] = hit.collider.gameObject.GetComponent<Hexagon>();
            }
        }
    }

    private static void DeselectAll()
    {
        foreach (var item in HexagonMapper.allHexagons)
        {
            if (item != null)
            {
                item.GetComponent<Hexagon>().Deselected();
            }
        }
    }

    public static Vector2 GetDirectionVector2D(float angle)
    {
        return new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad)).normalized;
    }
}
